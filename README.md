# University of Cambridge Language Centre, Russian Learning resources.

This is the repository for the Russian Learning resources built by the Language Centre at the University of Cambridge.

You can view these resources at:

http://lcdev.langcen.cam.ac.uk/projects/russian/index.html

2014 © University of Cambridge. Developed by the Language Centre.

This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivs 2.0 UK: England & Wales Licence.
To view a copy of this licence, visit: http://www.creativecommons.org

Pedram Badakhchani